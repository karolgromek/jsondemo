﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace JsonDemo.Common
{
    public class ObjJsonBase
    {
        [JsonProperty(Order = 1)]
        public string id { get; set; }

        [JsonProperty(Order = 2)]
        public List<object> fg { get; set; } = new List<object>();

        [JsonProperty(Order = 3)]
        public List<object> bg { get; set; } = new List<object>();

        [JsonProperty(Order = 4)]
        public bool? rotates { get; set; }
    }
}
