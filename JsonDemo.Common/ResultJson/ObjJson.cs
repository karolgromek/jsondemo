﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace JsonDemo.Common
{
    public class ObjJson : ObjJsonBase
    {
        [JsonIgnore]
        public string fileName;

        [JsonProperty(Order = 5)]
        public bool? animated { get; set; }

        [JsonProperty(Order = 6)]
        public bool? multitile { get; set; }

        [JsonProperty(Order = 7)]
        public List<ObjJsonBase> additional_tiles { get; set; }

        public void AddAdditionalTile(ObjJsonBase obj)
        {
            if (additional_tiles == null)
                additional_tiles = new List<ObjJsonBase>();
            additional_tiles.Add(obj);
        }
    }
}
