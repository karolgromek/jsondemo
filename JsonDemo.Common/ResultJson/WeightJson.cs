﻿namespace JsonDemo.Common
{
    public class WeightJson
    {
        public long weight { get; set; }
        public string sprite { get; set; }
    }
}
