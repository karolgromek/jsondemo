﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JsonDemo.Common
{
    public static class HashCollections
    {
        public static Dictionary<string, ISet<string>> DictionaryBG = new Dictionary<string, ISet<string>>();
        public static HashSet<string> FoundBG = new HashSet<string>();
        public static HashSet<string> CopiedName = new HashSet<string>();
        public static IEnumerable<string> NotFoundBG => DictionaryBG.Keys.Except(FoundBG);
    }
}
