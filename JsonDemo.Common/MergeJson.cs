﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;

namespace JsonDemo.Common
{
    public class MergeJson
    {
        IEnumerable<string> _inputDirs = new string[0];
        bool _processBG = false;
        string _outputRootDir = "";
        List<ObjJson> _jsonObjs = new List<ObjJson>();

        string _outputJson => Path.Combine(_outputRootDir, "output.json");

        public MergeJson(IEnumerable<string> inputDirs, bool processBG)
        {
            _inputDirs = inputDirs;
            _processBG = processBG;
        }

        public void MergeJsons()
        {
            Log.WriteLog();
            Log.WriteLog("START MERGING...");
            Log.WriteLog();

            foreach (string inputDir in _inputDirs)
            {
                _outputRootDir = inputDir;

                DeletePreviousOutputJson();

                string[] files = Directory.GetFiles(_outputRootDir, "*.json", SearchOption.AllDirectories);

                foreach (var file in files)
                    ProcessFile(file);

                WriteOutputJson();

                _jsonObjs.Clear();
            }

            Log.WriteLog();
            Log.WriteLog("END OF MERGING.");
        }

        void DeletePreviousOutputJson()
        {
            if (File.Exists(_outputJson))
                File.Delete(_outputJson);
        }

        void ProcessFile(string file)
        {
            var jsonData = File.ReadAllText(file);

            var jsonObj = JsonConvert.DeserializeObject<ObjJson>(jsonData);

            _jsonObjs.Add(jsonObj);

            ProcessBG(jsonObj);

            Log.WriteLog($"File merged '{file}'.");
        }

        void ProcessBG(ObjJson jsonObj)
        {
            if (!_processBG) return;

            if (!jsonObj.bg.Empty())
                ProcessBG(jsonObj.bg);

            if (!jsonObj.additional_tiles.Empty())
            {
                foreach (var additional_tile in jsonObj.additional_tiles)
                {
                    if (!additional_tile.bg.Empty())
                        ProcessBG(additional_tile.bg);
                }
            }
        }

        void ProcessBG(List<object> bgs)
        {
            foreach (var bg in bgs)
            {
                string s = "";

                switch (bg)
                {
                    case string str:
                        s = str;
                        break;
                    case JToken jToken:
                        s = jToken.ToObject<WeightJson>().sprite;
                        break;
                }

                if (!String.IsNullOrEmpty(s) && !HashCollections.CopiedName.Contains(s))
                {
                    if (HashCollections.DictionaryBG.ContainsKey(s))
                        HashCollections.DictionaryBG[s].Add(_outputRootDir);
                    else
                        HashCollections.DictionaryBG.Add(s, new HashSet<string>(new string[1] { _outputRootDir }));
                }
            }
        }

        void WriteOutputJson()
        {
            var setting = new JsonSerializerSettings
            {
                Formatting = Formatting.Indented,
                NullValueHandling = NullValueHandling.Ignore
            };
            var jsonData = JsonConvert.SerializeObject(_jsonObjs, setting);
            File.WriteAllText(_outputJson, jsonData);

            Log.WriteLog();
            Log.WriteLog($"Write output '{_outputJson}'.");
        }
    }
}
