﻿using System;
using System.IO;
using System.Threading;

namespace JsonDemo.Common
{
    public class Log
    {
        static object _logLock = new object();

        public static void WriteLog(string txt = "", string file = "log.txt")
        {
            Monitor.Enter(_logLock);

            if (String.IsNullOrEmpty(txt))
                File.AppendAllLines(file, new string[] { "" });
            else
                File.AppendAllLines(file, new string[] { $"{DateTime.Now:yyyy-MM-dd HH:mm:ss} - {txt}" });

            Monitor.Exit(_logLock);
        }

        public static void CleanLog(string file = "log.txt")
        {
            Monitor.Enter(_logLock);
            
            if (File.Exists(file))
                File.Delete(file);

            Monitor.Exit(_logLock);
        }
    }
}
