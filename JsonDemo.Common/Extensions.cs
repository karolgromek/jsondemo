﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace JsonDemo.Common
{
    public static class Extensions
    {
        public static bool Empty<T>(this IEnumerable<T> collection)
        {
            return !collection?.Any() ?? true;
        }

        public static long ToLong(this string str)
        {
            if (Int64.TryParse(str, out long l))
                return l;
            return 0L;
        }
    }
}
