﻿using System.IO;
using System.Threading;

namespace JsonDemo.Common
{
    public class DirectoryHelper
    {
        static object _dirLock = new object();

        public static string CreateDirectory(string path)
        {
            Monitor.Enter(_dirLock);

            if (Directory.Exists(path))
            {
                int c = 0;
                while (Directory.Exists($"{path}_{++c}"))
                { }
                path = $"{path}_{c}";
            }

            Directory.CreateDirectory(path);

            Monitor.Exit(_dirLock);

            return path;
        }
    }
}
