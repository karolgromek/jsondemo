﻿using JsonDemo.Common;
using System;
using System.IO;

namespace JsonDemoStep2
{
    class Program
    {
        const string _inputDir = "input_objects";
        const string _inputJson = "input_json";

        static JsonWorker _worker = new JsonWorker();

        static bool _onlyStage2 = false;
        static string _dicPath;
        static bool _fillFromDictionary => !String.IsNullOrEmpty(_dicPath);

        static void Main(string[] args)
        {
            SetArgsMode(args);

            Log.CleanLog();

            if (!Validate()) return;

            if (!_onlyStage2)
            {
                _worker.WriteAllIdToLogIfDebug();

                _worker.PrepareOutputDirectory();

                _worker.CopyFromInput(_inputDir);
            }

            _worker.MergeJsons();

            _worker.CopyBackground(_inputDir);
        }

        static void SetArgsMode(string[] args)
        {
            if (!args.Empty())
            {
                for (int i = 0; i < args.Length; i++)
                {
                    switch (args[i])
                    {
                        case "-?":
                        case "-h":
                        case "--help":
                            ExitWithHelp(null);
                            break;
                        case "--dictionary":
                            var dicPath = args[++i];
                            if (!String.IsNullOrEmpty(dicPath) && File.Exists(dicPath))
                                _dicPath = dicPath;
                            else
                                ExitWithHelp($"Invalid argument value: {dicPath}");
                            break;
                        case "--list-id":
                            _worker.Debug = true;
                            break;
                        case "--only-stage2":
                            _onlyStage2 = true;
                            break;
                        case "--separate-dir-output":
                            _worker.SeparateDirOutput = true;
                            break;
                        default:
                            ExitWithHelp($"Invalid argument: {args[i]}");
                            break;
                    }
                }
            }
        }

        static void ExitWithHelp(string error)
        {
            if (!String.IsNullOrEmpty(error))
            {
                Console.WriteLine();
                Console.WriteLine($"Argument error: {error}");
            }

            Console.WriteLine();
            Console.WriteLine("Usage:");
            Console.WriteLine("  --dictionary <dictionary_path> - specify override for id from json data");
            Console.WriteLine("  --list-id                      - listing all id from json data");
            Console.WriteLine("  --only-stage2                  - only merge json files and copy backgrounds");
            Console.WriteLine("  --separate-dir-output          - output to separate directories");
            Console.WriteLine("                                   'output_objects_<input_dir_name>'");
            Console.WriteLine();
            Console.WriteLine("Dictionary example:");
            Console.WriteLine();
            Console.WriteLine("dirt:item\\ammo");
            Console.WriteLine("dirt_1:item\\gun");
            Console.WriteLine("dirt_2:item\\tool\\cooking");
            Console.WriteLine();

            Environment.Exit(String.IsNullOrEmpty(error) ? 0 : 1);
        }

        static bool Validate()
        {
            if (!Directory.Exists(_inputDir))
            {
                Log.WriteLog($"ERROR - Directory not found: '{_inputDir}'.");
                return false;
            }

            if (Directory.EnumerateDirectories(_inputDir).Empty())
            {
                Log.WriteLog($"ERROR - Directory is empty: '{_inputDir}'.");
                return false;
            }

            if (!Directory.Exists(_inputJson))
            {
                Log.WriteLog($"ERROR - Directory not found: '{_inputJson}'.");
                return false;
            }

            if (Directory.EnumerateDirectories(_inputJson).Empty())
            {
                Log.WriteLog($"ERROR - Directory is empty: '{_inputJson}'.");
                return false;
            }

            if (_fillFromDictionary)
                _worker.FillFromDictionary(_dicPath);

            _worker.ListJsonDirs(_inputJson);

            if (_worker.Empty)
            {
                Log.WriteLog($"ERROR - Couldn'd find any valid 'id'.");
                return false;
            }

            return true;
        }
    }
}
