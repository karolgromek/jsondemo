﻿using JsonDemo.Common;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;

namespace JsonDemoStep2
{
    class JsonWorker
    {
        const string _outputRootDir = "output_objects";
        const string _outputDir = "pngs";

        bool _debug = false;
        bool _separateDirOutput = false;
        bool _readingInputDictionary = false;

        string[] _prefix = {
            "overlay_mutation_" ,
            "overlay_female_mutation_" ,
            "overlay_male_mutation_",
            "overlay_worn_" ,
            "overlay_female_worn_" ,
            "overlay_male_worn_",
            "overlay_wielded_" ,
            "overlay_female_wielded_" ,
            "overlay_male_wielded_",
            "overlay_effect_",
            "vp_",
            "rid_",
            "corpse_"
        };

        string[] _suffix = {
            "_season_spring",
            "_season_summer",
            "_season_autumn",
            "_season_winter"
        };

        string[] _offsetStartsWithDirNames = {
            "7_expan2",
            "17_top_hats"
        };

        Regex _sizePatt = new Regex("\\d+x\\d+");
        Regex _nonLetterPatt = new Regex("[^\\w+]");

        Dictionary<string, string> _dic = new Dictionary<string, string>();
        ISet<string> _outputDirs = new HashSet<string>();

        internal bool Empty => _dic.Empty();

        internal bool Debug { set => _debug = value; }
        internal bool SeparateDirOutput { set => _separateDirOutput = value; }

        internal void FillFromDictionary(string dicPath)
        {
            IEnumerable<string> dicText = File.ReadAllLines(dicPath);

            if (dicText.Empty()) return;

            _readingInputDictionary = true;

            dicText = dicText.Where(x => !String.IsNullOrEmpty(x));

            var dic = dicText
                .Select(x => x.Split(':'))
                .Where(x => x.Length == 2)
                .Where(x => !String.IsNullOrEmpty(x[0]) && !String.IsNullOrEmpty(x[1]));

            foreach (var kvp in dic)
            {
                if (!_dic.ContainsKey(kvp[0]))
                    _dic.Add(kvp[0], PathCombine(kvp[1], kvp[0], skip: 0));
            }

            _readingInputDictionary = false;
        }

        internal void ListJsonDirs(string dir)
        {
            ListJsonFiles(dir);

            var dirs = Directory.GetDirectories(dir);
            foreach (var d in dirs)
            {
                ListJsonFiles(d);

                ListJsonDirs(d);
            }
        }

        void ListJsonFiles(string dir)
        {
            var files = Directory.GetFiles(dir);
            foreach (var filePath in files)
            {
                if (!ReadJson(filePath, out List<JsonObj> jsonObjs))
                {
                    Log.WriteLog(txt: $"ERROR - Couldn't deserialize file '{filePath}'");
                    continue;
                }

                if (jsonObjs.Empty())
                {
                    Log.WriteLog(txt: $"ERROR - Couldn't find any 'id' in file '{filePath}'");
                    continue;
                }

                string fileName;
                if (IsSpecialPath(filePath))
                    fileName = GetSpecialFileName(filePath);
                else
                    fileName = Path.GetFileNameWithoutExtension(filePath);

                var myObjs = jsonObjs.Select(x => new MyObj(x)).ToList();

                if (!myObjs.Any(x => x.ids?.Contains(fileName) ?? false))
                    myObjs.Add(new MyObj(fileName));

                foreach (var myObj in myObjs)
                {
                    if (myObj.ids.Empty())
                    {
                        Log.WriteLog($"ERROR - Unknown 'id' type in file '{filePath}'");
                        continue;
                    }

                    foreach (var id in myObj.ids)
                    {
                        if (String.IsNullOrEmpty(id))
                        {
                            Log.WriteLog(txt: $"ERROR - Empty 'id' in file '{filePath}'");
                            continue;
                        }

                        if (!_dic.ContainsKey(id))
                            _dic.Add(id, PathCombine(filePath, id));

                        foreach (var p in _prefix)
                        {
                            string key = $"{p}{id}";
                            if (!_dic.ContainsKey(key))
                                _dic.Add(key, PathCombine(filePath, id, prefix: p));
                        }

                        foreach (var p in _suffix)
                        {
                            string key = $"{id}{p}";
                            if (!_dic.ContainsKey(key))
                                _dic.Add(key, PathCombine(filePath, id, suffix: p));
                        }
                    }
                }
            }
        }

        bool ReadJson(string path, out List<JsonObj> jsonObjs)
        {
            jsonObjs = null;

            try
            {
                var fileData = File.ReadAllText(path);

                jsonObjs = JsonConvert.DeserializeObject<List<JsonObj>>(fileData);

                jsonObjs = jsonObjs.Where(x => x.id != null).ToList();

                return true;
            }
            catch
            { }

            return false;
        }

        string PathCombine(string filePath, string id, string prefix = null, string suffix = null, int skip = 1)
        {
            string fileName = Path.GetFileNameWithoutExtension(filePath);
            string[] pathInputParts = filePath.Split(Path.DirectorySeparatorChar);
            List<string> pathOutputLst = StartGenerateOutputPath(pathInputParts);

            if (IsSpecialPath(pathInputParts))
            {
                pathOutputLst.AddRange(pathInputParts.Skip(2).Reverse().Skip(1).Reverse());

                var fileParts = fileName.Split('-');
                pathOutputLst.Add(fileParts[0]);
                pathOutputLst.Add(String.Join("-", fileParts.Skip(1)));
            }
            else if (_separateDirOutput && skip == 1)
            {
                pathOutputLst.AddRange(pathInputParts.Skip(2).Reverse().Skip(1).Reverse());
                pathOutputLst.Add(fileName);
            }
            else
            {
                pathOutputLst.AddRange(pathInputParts.Skip(skip).Reverse().Skip(1).Reverse());
                pathOutputLst.Add(fileName);
            }

            pathOutputLst.Add(id);

            if (!String.IsNullOrEmpty(prefix))
                pathOutputLst.Add($"{prefix}{id}");

            if (!String.IsNullOrEmpty(suffix))
                pathOutputLst.Add($"{id}{suffix}");

            return Path.Combine(pathOutputLst.ToArray());
        }

        List<string> StartGenerateOutputPath(string[] pathInputParts)
        {
            string outputRootDir = (_separateDirOutput, _readingInputDictionary) switch
            {
                (true, false) => AddSuffixToOutputFirstPathElement(pathInputParts),
                _ => _outputRootDir
            };

            _outputDirs.Add(outputRootDir);

            List<string> pathOutputLst = new List<string> { outputRootDir, _outputDir };

            return pathOutputLst;
        }

        string AddSuffixToOutputFirstPathElement(string[] pathInputParts)
        {
            string dirName = pathInputParts.Reverse().Skip(1).Reverse().Skip(1).FirstOrDefault();

            if (!String.IsNullOrEmpty(dirName))
            {
                dirName = _nonLetterPatt.Replace(dirName, "_");

                return $"{_outputRootDir}_{dirName}";
            }

            return _outputRootDir;
        }

        bool IsSpecialPath(string filePath)
        {
            var pathParts = filePath.Split(Path.DirectorySeparatorChar);
            return IsSpecialPath(pathParts);
        }

        bool IsSpecialPath(string[] pathParts)
        {
            return pathParts.Contains("furniture_and_terrain");
        }

        string GetSpecialFileName(string filePath)
        {
            var fileName = Path.GetFileNameWithoutExtension(filePath);
            var fileParts = fileName.Split('-');
            return String.Join("-", fileParts.Skip(1));
        }

        internal void WriteAllIdToLogIfDebug()
        {
            if (_debug)
            {
                Log.WriteLog();
                Log.WriteLog("LISTING ALL ID FROM JSON DATA...");
                Log.WriteLog();

                var max = _dic.Max(x => x.Key.Length);
                var items = _dic.OrderBy(x => x.Key);
                foreach (var kvp in items)
                {
                    Log.WriteLog($"{kvp.Key.PadRight(max)} - {kvp.Value}");
                }

                Log.WriteLog();
                Log.WriteLog("END OF LISTING.");
                Log.WriteLog();
            }
        }

        internal void PrepareOutputDirectory()
        {
            foreach (string outputDir in _outputDirs)
            {
                if (Directory.Exists(outputDir))
                    Directory.Delete(outputDir, true);

                Directory.CreateDirectory(outputDir);
            }
        }

        internal void CopyFromInput(string inputDir)
        {
            Log.WriteLog();
            Log.WriteLog("START COPYING...");
            Log.WriteLog();

            var rootInputDirs = Directory.GetDirectories(inputDir);

            foreach (var d in rootInputDirs)
                CopyInputDirs(d);

            Log.WriteLog();
            Log.WriteLog("END OF COPYING.");
        }

        void CopyInputDirs(string rootInputDir)
        {
            bool isOffsetDirName = IsOffsetDirName(rootInputDir);
            var size = _sizePatt.Match(rootInputDir).Value;

            var inputDirs = Directory.GetDirectories(rootInputDir);

            foreach (var inputDir in inputDirs)
            {
                var dirName = inputDir.Split(Path.DirectorySeparatorChar).Last();

                if (!_dic.ContainsKey(dirName))
                {
                    Log.WriteLog($"WARNING - Directory won't be copied '{inputDir}'.");
                    continue;
                }

                var path = _dic[dirName];

                IncludesToDirName(path, isOffsetDirName, size, out string outputDir);

                CreateDirectory(outputDir);

                CopyFiles(inputDir, outputDir);

                HashCollections.CopiedName.Add(dirName);
            }
        }

        bool IsOffsetDirName(string dir)
        {
            var pathParts = dir.Split(Path.DirectorySeparatorChar);
            var dirName = pathParts.Skip(1).FirstOrDefault();
            if (String.IsNullOrEmpty(dirName))
                return false;
            if (_offsetStartsWithDirNames.Any(x => dirName.StartsWith(x)))
                return true;
            return false;
        }

        void IncludesToDirName(string path, bool isOffsetDirName, string size, out string outputDir)
        {
            var pathParts = path.Split(Path.DirectorySeparatorChar);

            if (isOffsetDirName)
                pathParts[1] = $"{pathParts[1]}_offset";

            pathParts[1] = $"{pathParts[1]}_{size}";

            outputDir = Path.Combine(pathParts);
        }

        void CreateDirectory(string path)
        {
            if (!Directory.Exists(path))
                Directory.CreateDirectory(path);
        }

        void CopyFiles(string inputDir, string outputDir)
        {
            var files = Directory.GetFiles(inputDir);

            foreach (var file in files)
            {
                var fileName = Path.GetFileName(file);
                
                var destFile = Path.Combine(outputDir, fileName);

                File.Copy(file, destFile, true);

                Log.WriteLog($"File copied - '{file}' => '{destFile}'");
            }
        }

        internal void MergeJsons()
        {
            var merger = new MergeJson(_outputDirs, true);
            merger.MergeJsons();
        }

        internal void CopyBackground(string inputDir)
        {
            Log.WriteLog();
            Log.WriteLog("START COPYING BACKGROUNDS...");
            Log.WriteLog();

            ProcessCopyBG(inputDir);

            LogNotFoundDirectory();

            Log.WriteLog();
            Log.WriteLog("END OF COPYING BACKGROUNDS.");
        }

        void ProcessCopyBG(string inputDir)
        {
            bool isOffsetDirName = IsOffsetDirName(inputDir);
            var dirs = Directory.GetDirectories(inputDir);

            foreach (var dir in dirs)
            {
                ProcessCopyBG(dir);

                var dirName = dir.Split(Path.DirectorySeparatorChar).LastOrDefault();

                if (!HashCollections.DictionaryBG.ContainsKey(dirName))
                    continue;

                var dirParts = dir.Split(Path.DirectorySeparatorChar);
                var size = _sizePatt.Match(dirParts.Skip(1).FirstOrDefault()).Value;

                foreach (string bgPath in HashCollections.DictionaryBG[dirName])
                {
                    var outputPath = Path.Combine(bgPath, _outputDir, "background", dirName);

                    IncludesToDirName(outputPath, isOffsetDirName, size, out string outputDir);

                    CreateDirectory(outputDir);

                    CopyFiles(dir, outputDir);
                }

                HashCollections.FoundBG.Add(dirName);
            }
        }

        void LogNotFoundDirectory()
        {
            foreach (var bg in HashCollections.NotFoundBG)
                Log.WriteLog($"WARNING - Background not found '{bg}'.");
        }
    }
}
