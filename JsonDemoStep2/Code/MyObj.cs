﻿using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.Linq;

namespace JsonDemoStep2
{
    class MyObj
    {
        public List<string> ids { get; set; }

        internal MyObj(string fileName)
        {
            ids = new List<string> { fileName };
        }

        internal MyObj(JsonObj obj)
        {
            if (obj.id is string str)
                ids = new List<string> { str };
            else if (obj.id is JArray arr)
                ids = arr.Select(x => x.Value<string>()).ToList();
        }
    }
}
