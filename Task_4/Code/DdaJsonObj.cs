﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task_4
{
    public class DdaJsonObj
    {
        public string id { get; set; }
        public object fg { get; set; }
        public object bg { get; set; }
        public bool? rotates { get; set; }
        public bool? animated { get; set; }
        public bool? multitile { get; set; }
        public object additional_tiles { get; set; }
    }
}
