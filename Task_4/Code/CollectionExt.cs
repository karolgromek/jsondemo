﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Task_4
{
    internal static class CollectionExt
    {
        internal static bool Empty<T>(this IEnumerable<T> items)
            => items == null || !items.Any();

        internal static bool NotEmpty<T>(this IEnumerable<T> items)
            => !items.Empty();
    }
}
