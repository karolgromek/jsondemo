﻿using Newtonsoft.Json;

namespace Task_4
{
    public class DataJsonObj
    {
        public string id { get; set; }

        [JsonProperty("abstract")]
        public string @abstract { get; set; }

        [JsonProperty("looks_like")]
        public string looksLike { get; set; }

        [JsonProperty("copy-from")]
        public string copyFrom { get; set; }
    }
}
