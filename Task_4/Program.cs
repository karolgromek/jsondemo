﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Task_4
{
    class Program
    {
        const string _wyjsciowyKatalog = "output";

        static string[] _missingIds = new string[0];
        static string _missingIdsPath = "";
        static string _ddaPath = "";
        static string _dataJsonPath = "";
        static bool _verbose = false;

        static string _indent = $"{Environment.NewLine}  ";

        static Dictionary<string, List<string>> _dataJsonDic = new Dictionary<string, List<string>>();
        static Dictionary<string, List<string>> _ddaDic = new Dictionary<string, List<string>>();

        static JsonSerializerSettings _jsonSerializerSettings = new JsonSerializerSettings
        {
            Formatting = Formatting.Indented,
            NullValueHandling = NullValueHandling.Ignore
        };

        static void Main(string[] args)
        {
            OdczytajParametry(args);

            PrzygotujOutput();

            WyswietlOdczytanaListe();

            OdczytajDataJson();

            WyswietlDataJson();

            OdczytajDda();

            WyswietlDda();

            SkopiujBrakujaceId();

            Log("Koniec.");
        }

        static void OdczytajParametry(string[] args)
        {
            Console.WriteLine("Task4 - Copyright 2020 SzuruBuru Inc.");
            Console.WriteLine("### Dodaj se opis co to robi ;) ###");
            Console.WriteLine();

            if (args.Empty())
                WyswietlPomoc(error: "Program działa tylko z argumentami");

            try
            {
                OdczytajParametryImpl(args);
            }
            catch (Exception ex)
            {
                WyswietlPomoc(error: "Jakiś błąd: " + ex.Message);
            }
        }

        static void OdczytajParametryImpl(string[] args)
        {
            for (int i = 0; i < args.Length; i++)
            {
                string path = "";
                string cmd = args[i];
                switch (cmd)
                {
                    case "--missing-ids-path":
                        path = args[++i];
                        if (String.IsNullOrEmpty(path))
                            WyswietlPomoc(error: "Ścieżka nie może być pusta dla argumentu: " + cmd);
                        if (!File.Exists(path))
                            WyswietlPomoc(error: "Plik nie istnieje: " + path);
                        _missingIdsPath = path;
                        _missingIds = File.ReadAllLines(path);
                        break;
                    case "--dda-path":
                        path = args[++i];
                        if (String.IsNullOrEmpty(path))
                            WyswietlPomoc(error: "Ścieżka nie może być pusta dla argumentu: " + cmd);
                        if (!Directory.Exists(path))
                            WyswietlPomoc(error: "Katalog nie istnieje: " + path);
                        _ddaPath = path;
                        break;
                    case "--data-json-path":
                        path = args[++i];
                        if (String.IsNullOrEmpty(path))
                            WyswietlPomoc(error: "Ścieżka nie może być pusta dla argumentu: " + cmd);
                        if (!Directory.Exists(path))
                            WyswietlPomoc(error: "Katalog nie istnieje: " + path);
                        _dataJsonPath = path;
                        break;
                    case "--verbose":
                        _verbose = true;
                        break;
                    default:
                        WyswietlPomoc(error: "Nie rozpoznany argument: " + cmd);
                        break;
                }
            }
        }

        static void WyswietlPomoc(string error = null)
        {
            if (!String.IsNullOrEmpty(error))
            {
                Console.WriteLine(error);
                Console.WriteLine();
            }

            Console.WriteLine("Argumenty:");
            Console.WriteLine("  --missing-ids-path <path> - ścieżka do pliku z listą płaską id'ków");
            Console.WriteLine("  --dda-path <path>         - ścieżka do katalogu, który nie pamiętam co zawiera");
            Console.WriteLine("  --data-json-path <path>   - ścieżka do katalogu z plikami json");
            Console.WriteLine("  --verbose                 - wyświetla wszystkie logi");
            Console.WriteLine();

            Environment.Exit(String.IsNullOrEmpty(error) ? 0 : 1);
        }

        static void Verbose(string txt = null)
        {
            if (!_verbose) return;
            Log(txt);
        }

        static void Log(string txt = null)
        {
            if (String.IsNullOrEmpty(txt))
                Console.WriteLine();
            else
                Console.WriteLine($"{DateTime.Now:yyyy-MM-dd HH:mm:ss.fff} - {txt}");
        }

        static void PrzygotujOutput()
        {
            Verbose();

            if (Directory.Exists(_wyjsciowyKatalog))
            {
                Verbose($"Usunięcie katalogu: {_wyjsciowyKatalog}");
                Directory.Delete(_wyjsciowyKatalog, true);
            }

            Verbose($"Utworzenie katalogu: {_wyjsciowyKatalog}");
            Directory.CreateDirectory(_wyjsciowyKatalog);

            Verbose();
        }

        static void WyswietlOdczytanaListe()
        {
            if (!_verbose) return;

            Log();
            Log($"Wartości odczytane z pliku: {_missingIdsPath}");
            Log();

            foreach (var id in _missingIds)
                Log(id);

            Log();
        }

        static void OdczytajDataJson()
        {
            var files = Directory.GetFiles(_dataJsonPath, "*.json", SearchOption.AllDirectories);

            foreach (var file in files)
            {
                Verbose($"Odczytywanie pliku: {file}");

                IEnumerable<DataJsonObj> objs = null;

                try
                {
                    string fileTxt = File.ReadAllText(file);
                    objs = JsonConvert.DeserializeObject<DataJsonObj[]>(fileTxt);
                }
                catch (Exception ex)
                {
                    Verbose($"Błąd: {ex.Message}");
                    continue;
                }

                if (objs.Empty())
                {
                    Verbose($"Błąd: Nie odczytano żadnego obiektu");
                    continue;
                }

                objs = objs
                    .Where(x => !String.IsNullOrEmpty(x.id) || !String.IsNullOrEmpty(x.@abstract))
                    .Where(x => !String.IsNullOrEmpty(x.looksLike) || !String.IsNullOrEmpty(x.copyFrom));

                if (objs.Empty())
                {
                    Verbose($"Błąd: Nie znaleziono żadnego pasującego obiektu");
                    continue;
                }

                foreach (var obj in objs)
                {
                    string id = !String.IsNullOrEmpty(obj.id) ? obj.id : obj.@abstract;

                    if (!String.IsNullOrEmpty(obj.looksLike))
                        AddToDataJsonDic(id, obj.looksLike);
                    else
                        AddToDataJsonDic(id, obj.copyFrom);
                }
            }
        }

        static void AddToDataJsonDic(string key, string value)
        {
            if (_dataJsonDic.ContainsKey(key))
                _dataJsonDic[key].Add(value);
            else
                _dataJsonDic.Add(key, new List<string> { value });
        }

        static void WyswietlDataJson()
        {
            if (!_verbose) return;

            Log();
            Log($"Wartości odczytane z katalogu: {_dataJsonPath}");
            Log();

            foreach (var kvp in _dataJsonDic)
                Log($"{kvp.Key}{_indent}{String.Join(_indent, kvp.Value)}");

            Log();
        }

        static void OdczytajDda()
        {
            var items = Directory.GetFiles(_ddaPath, "*.json", SearchOption.AllDirectories);

            foreach (var item in items)
            {
                string key = PobierzNazweKataloguZeSciezkiPliku(item);
                string val = Path.GetDirectoryName(item);
                if (_ddaDic.ContainsKey(key))
                    _ddaDic[key].Add(val);
                else
                    _ddaDic.Add(key, new List<string> { val });
            }
        }

        static string PobierzNazweKataloguZeSciezkiPliku(string dirPath)
        {
            if (String.IsNullOrEmpty(dirPath))
                return null;

            return dirPath.Split(Path.DirectorySeparatorChar, StringSplitOptions.RemoveEmptyEntries)
                .SkipLast(1)
                .LastOrDefault();
        }

        static void WyswietlDda()
        {
            if (!_verbose) return;

            Log();
            Log($"Wartości odczytane z katalogu: {_ddaPath}");
            Log();

            foreach (var kvp in _ddaDic)
                Log($"{kvp.Key}{_indent}{String.Join(_indent, kvp.Value)}");

            Log();
        }

        static void SkopiujBrakujaceId()
        {
            Log();
            Log("Kopiowanie");
            Log();
            IEnumerable<string> ids = _missingIds.Where(x => _dataJsonDic.ContainsKey(x));

            byte poziom = 10;

            foreach (string id in ids)
            {
                poziom = 10;

                IEnumerable<string> oldIds = GetOldIds(id, ref poziom);

                oldIds = oldIds.Where(ZnajdujeSieWDdaIJestPng);

                if (oldIds.Empty())
                {
                    Log($"Błąd: Brak katalogu lub pliku png dla id: {id}");
                    continue;
                }

                foreach (string oldId in oldIds)
                {
                    foreach (string sciezka in _ddaDic[oldId])
                    {
                        KopiujPliki(sciezka, oldId, id);
                    }
                }
            }

            Log();
        }

        static IEnumerable<string> GetOldIds(string id, ref byte poziom)
        {
            if (--poziom == 0) return new string[0];

            IEnumerable<string> oldIds = _dataJsonDic[id];

            if (!oldIds.Any(ZnajdujeSieWDdaIJestPng))
            {
                foreach (string oldId in oldIds)
                {
                    if (_dataJsonDic.ContainsKey(oldId))
                        return GetOldIds(oldId, ref poziom);
                }
            }

            return oldIds;
        }

        static bool ZnajdujeSieWDdaIJestPng(string oldId)
        {
            if (!_ddaDic.ContainsKey(oldId)) return false;

            foreach (string sciezka in _ddaDic[oldId])
            {
                if (Directory.GetFiles(sciezka, "*.png").NotEmpty())
                    return true;
            }

            return false;
        }

        static string GenerujSciezke(string path, string id)
        {
            string virtualPath = path.Replace(_ddaPath, "");

            string[] virtualPathParts = virtualPath.Split(Path.DirectorySeparatorChar, StringSplitOptions.RemoveEmptyEntries);

            virtualPath = Path.Combine(virtualPathParts
                .Prepend(_wyjsciowyKatalog)
                .SkipLast(1)
                .Append(id)
                .ToArray());

            return virtualPath;
        }

        static void KopiujPliki(string sciezka, string oldId, string id)
        {
            string sciezkaWyjsciowa = GenerujSciezke(sciezka, id);

            if (!Directory.Exists(sciezkaWyjsciowa))
                Directory.CreateDirectory(sciezkaWyjsciowa);

            string[] pliki = PobierzPlikiDoSkopiowania(sciezka);
            KopiujPliki(pliki, sciezkaWyjsciowa, oldId, id);

            string[] katalogi = PobierzKatalogiDoSkopiowania(sciezka);
            KopiujKatalogi(katalogi, sciezkaWyjsciowa, oldId, id);
        }

        static string[] PobierzPlikiDoSkopiowania(string sciezka)
        {
            return Directory.GetFiles(sciezka, "*.json");
        }

        static void KopiujPliki(string[] pliki, string sciezkaWyjsciowa, string oldId, string id)
        {
            foreach (string plik in pliki)
            {
                string nazwaPliku = Path.GetFileName(plik);
                string nowaNazwaPliku = nazwaPliku.Replace(oldId, id);
                string sciezkaPliku = Path.Combine(sciezkaWyjsciowa, nowaNazwaPliku);

                string zawartoscPliku = File.ReadAllText(plik);
                DdaJsonObj obj = JsonConvert.DeserializeObject<DdaJsonObj>(zawartoscPliku);
                obj.id = obj.id.Replace(oldId, id);

                string nowaZawartoscPliku = JsonConvert.SerializeObject(obj, _jsonSerializerSettings);
                File.WriteAllText(sciezkaPliku, nowaZawartoscPliku);
                Log($"Nowy plik: {oldId}: {id}: {sciezkaPliku}");
            }
        }

        static string[] PobierzKatalogiDoSkopiowania(string sciezka)
        {
            return Directory.GetDirectories(sciezka);
        }

        static void KopiujKatalogi(string[] katalogi, string sciezkaWyjsciowa, string oldId, string id)
        {
            foreach (string sciezka in katalogi)
            {
                string nazwaKatalogu = sciezka.Split(Path.DirectorySeparatorChar, StringSplitOptions.RemoveEmptyEntries).LastOrDefault();
                string nowaNazwaKatalogu = nazwaKatalogu.Replace(oldId, id);
                string nowaSciezka = Path.Combine(sciezkaWyjsciowa, nowaNazwaKatalogu);

                if (!Directory.Exists(nowaSciezka))
                {
                    Log($"Nowy katalog: {oldId}: {id}: {nowaSciezka}");
                    Directory.CreateDirectory(nowaSciezka);
                }

                string[] pliki = PobierzPlikiDoSkopiowania(sciezka);
                KopiujPliki(pliki, nowaSciezka, oldId, id);

                string[] podkatalogi = PobierzKatalogiDoSkopiowania(sciezka);
                KopiujKatalogi(podkatalogi, nowaSciezka, oldId, id);
            }
        }
    }
}
