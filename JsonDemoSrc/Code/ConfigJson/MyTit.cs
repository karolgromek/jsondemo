﻿using JsonDemo.Common;
using System.Linq;

namespace JsonDemo
{
    class MyTit : MyTitBase
    {
        internal bool? animated { get; set; }
        internal bool? multitile { get; set; }
        internal MyTitBase[] additional_tiles { get; set; }

        internal MyTit(Tit tit) : base(tit)
        {
            animated = tit.animated;
            multitile = tit.multitile;

            if (!tit.additional_tiles.Empty())
                additional_tiles = tit.additional_tiles.Select(x => new MyTitBase(x)).ToArray();
        }


    }
}
