﻿namespace JsonDemo
{
    class TitBase
    {
        public object id { get; set; }
        public object fg { get; set; }
        public object bg { get; set; }
        public bool? rotates { get; set; }
    }
}
