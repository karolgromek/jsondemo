﻿namespace JsonDemo
{
    class Tit : TitBase
    {
        public bool? animated { get; set; }
        public bool? multitile { get; set; }
        public TitBase[] additional_tiles { get; set; }
    }
}
