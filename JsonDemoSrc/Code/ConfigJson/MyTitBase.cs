﻿using JsonDemo.Common;
using System.Linq;
using Newtonsoft.Json.Linq;

namespace JsonDemo
{
    class MyTitBase
    {
        internal string[] id { get; set; }

        internal long[] fgIntArr { get; set; }
        internal WeightObj[] fgObjArr { get; set; }

        internal long[] bgIntArr { get; set; }
        internal WeightObj[] bgObjArr { get; set; }

        internal bool? rotates { get; set; }

        internal bool existID => !id.Empty();

        internal bool existFG => !fgIntArr.Empty() || !fgObjArr.Empty();
        internal bool existBG => !bgIntArr.Empty() || !bgObjArr.Empty();

        internal int countFG => fgIntArr?.Length ?? fgObjArr?.Length ?? 0;
        internal int countBG => bgIntArr?.Length ?? bgObjArr?.Length ?? 0;

        internal MyTitBase(TitBase tit)
        {
            if (tit.id is string str)
                id = new string[] { str };
            else if (tit.id is JArray arr)
                id = ConvertArr(arr).Cast<string>().ToArray();

            if (tit.fg != null)
            {
                if (tit.fg is long l)
                    fgIntArr = new long[] { l };
                else if (tit.fg is JArray arr)
                {
                    var objArr = ConvertArr(arr);
                    var intArr = objArr.OfType<long>();
                    if (intArr.Empty())
                        fgObjArr = objArr.Cast<WeightObj>().ToArray();
                    else
                        fgIntArr = intArr.ToArray();
                }
            }

            if (tit.bg != null)
            {
                if (tit.bg is long l)
                    bgIntArr = new long[] { l };
                else if (tit.bg is JArray arr)
                {
                    var objArr = ConvertArr(arr);
                    var intArr = objArr.OfType<long>();
                    if (intArr.Empty())
                        bgObjArr = objArr.Cast<WeightObj>().ToArray();
                    else
                        bgIntArr = intArr.ToArray();
                }
            }

            rotates = tit.rotates;
        }

        object[] ConvertArr(JArray arr)
        {
            return arr.Select(ConvertObj).ToArray();
        }

        object ConvertObj(JToken obj)
        {
            switch (obj.Type)
            {
                case JTokenType.Array:
                    return obj.Select(ConvertObj).ToArray();
                case JTokenType.String:
                    return obj.Value<string>();
                case JTokenType.Integer:
                    return obj.Value<long>();
                case JTokenType.Object:
                    return obj.ToObject<WeightObj>();
            }

            return null;
        }
    }
}
