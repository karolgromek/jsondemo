﻿using System.Collections.Concurrent;
using System.Linq;
using System.Threading;

namespace JsonDemo
{
    class FileNameContainer
    {
        static ReaderWriterLockSlim _rwl = new ReaderWriterLockSlim();

        static OrderablePartitioner<IGrouping<long, string>> _fileNameLst;

        internal static OrderablePartitioner<IGrouping<long, string>> FileNameLst
        {
            get
            {
                _rwl.EnterReadLock();
                try
                {
                    return _fileNameLst;
                }
                finally
                {
                    _rwl.ExitReadLock();
                }
            }
            set
            {
                _rwl.EnterWriteLock();
                try
                {
                    _fileNameLst = value;
                }
                finally
                {
                    _rwl.ExitWriteLock();
                }
            }
        }

        ~FileNameContainer()
        {
            if (_rwl != null) _rwl.Dispose();
        }
    }
}
