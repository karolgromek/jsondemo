﻿using JsonDemo.Common;
using Newtonsoft.Json;
using System;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace JsonDemo
{
    class TitWorker
    {
        string _guid = Guid.NewGuid().ToString("N").Substring(0, 8);

        MyTit _myTit;
        MyTitBase _currentTit;

        string _fileExt;
        string _currentMainTileName;
        string _currentTileName;
        string _currentPath;
        int _NewTileCount;
        bool _additionalTits;

        ObjJson _objJson;
        ObjJsonBase __currentObjJson;
        ObjJsonBase _currentObjJson
        {
            set => __currentObjJson = value;
            get => _additionalTits ? __currentObjJson : _objJson;
        }

        JsonSerializerSettings _settings = new JsonSerializerSettings
        {
            NullValueHandling = NullValueHandling.Ignore,
            Formatting = Formatting.Indented
        };

        internal string[] MainId => _myTit.id;

        internal TitWorker(Tit tit)
        {
            _myTit = new MyTit(tit);
        }

        void WriteLog(string txt)
        {
            Log.WriteLog($"[{_guid}] - {txt}");
        }

        internal bool ValidateID(string[] idMustContainLst)
        {
            if (_myTit.id.Empty() || _myTit.id.Any(x => String.IsNullOrEmpty(x)))
            {
                WriteLog("ERROR - Invalid \"id\"");
                return false;
            }

            if (!idMustContainLst.Empty())
            {
                var id = _myTit.id.Where(z => idMustContainLst.Any(y => z.Contains(y)));
                if (id.Empty())
                {
                    WriteLog($"INFO - \"id\" doesn't match 'must contain list',  id: {String.Join(", ", _myTit.id)}");
                    return false;
                }

                _myTit.id = id.ToArray();
            }

            return true;
        }

        internal bool ValidateFG()
        {
            if (!_myTit.existFG)
            {
                WriteLog("Empty \"fg\"");
                return false;
            }

            return true;
        }

        internal bool ValidateBG()
        {
            if (!_myTit.existBG)
            {
                WriteLog("Empty \"bg\"");
                return false;
            }

            return true;
        }

        internal void SetFileExtention(string fileExt)
        {
            _fileExt = fileExt;
        }

        internal void SetCurrentTile(string tileName)
        {
            _currentMainTileName = tileName;
            _currentTileName = tileName;
        }

        internal bool IsInvalidDirectory()
        {
            if (_currentTileName.Any(x => Path.GetInvalidFileNameChars().Contains(x)))
            {
                WriteLog($"ERROR - Can't create directory: '{_currentTileName}'");
                return true;
            }

            return false;
        }

        internal void CreateJsonObj()
        {
            _objJson = new ObjJson
            {
                fileName = $"{_currentMainTileName}.json",
                rotates = _myTit.rotates,
                animated = _myTit.animated,
                multitile = _myTit.multitile
            };
        }

        internal void SetCurrentPath(string path)
        {
            _currentPath = DirectoryHelper.CreateDirectory(path);
        }

        internal void LogNewTileName()
        {
            WriteLog($"New tile name: {_currentMainTileName}");
        }

        internal void ProcessMainTit()
        {
            _additionalTits = false;

            ProcessTit(_myTit);

            SetObjJsonIdAndAddAdditionalTile();

            if (!_myTit.additional_tiles.Empty())
            {
                _additionalTits = true;

                foreach (var tit in _myTit.additional_tiles)
                {
                    _currentObjJson = new ObjJsonBase
                    {
                        rotates = tit.rotates
                    };

                    foreach (var titId in tit.id)
                    {
                        _currentTileName = titId;

                        ProcessTit(tit);

                        SetObjJsonIdAndAddAdditionalTile();
                    }
                }
            }
        }

        void ProcessTit(MyTitBase tit)
        {
            _currentTit = tit;

            NewTileCount();

            if (_currentTit.existFG)
                ProcessFG();
            else if (_currentTit.existBG)
                ProcessBG();

            if (_currentTit.existFG && _currentTit.existBG)
                ProcessBgWithPairs();
        }

        void NewTileCount()
        {
            _NewTileCount = _currentTit.countFG;

            if (_NewTileCount == 0)
                _NewTileCount = _currentTit.countBG;
        }

        void ProcessFG()
        {
            if (!_currentTit.fgIntArr.Empty())
                ProcessInt(_currentTit.fgIntArr);
            else if (!_currentTit.fgObjArr.Empty())
                ProcessObj(_currentTit.fgObjArr);
        }

        void ProcessBG()
        {
            if (!_currentTit.bgIntArr.Empty())
                ProcessInt(_currentTit.bgIntArr);
            else if (!_currentTit.bgObjArr.Empty())
                ProcessObj(_currentTit.bgObjArr);
        }

        void ProcessInt(long[] arr)
        {
            string newTileName;
            if (_NewTileCount == 1)
            {
                newTileName = GetTileFileName();
                if (ProcessImg(arr[0], newTileName: newTileName))
                    AddFbObjJsonItem(newTileName: newTileName);
            }
            else
            {
                int c = _currentObjJson.fg.Count;
                foreach (var number in arr)
                {
                    newTileName = GetTileFileName(++c);
                    if (ProcessImg(number, newTileName: newTileName))
                        AddFbObjJsonItem(newTileName: newTileName);
                }
            }
        }

        void ProcessObj(WeightObj[] arr)
        {
            string newTileName;
            if (_NewTileCount == 1)
            {
                newTileName = GetTileFileName();
                //if (ProcessImg(arr[0].sprite, newTileName: newTileName))
                //    AddFbObjJsonItem(newTileName: newTileName, weight: arr[0].weight);
                if (ProcessImg(arr[0].sprite, newTileName: newTileName))
                    AddFbObjJsonItem(newTileName: newTileName);
            }
            else
            {
                int c = _currentObjJson.fg.Count;
                foreach (var obj in arr)
                {
                    newTileName = GetTileFileName(++c);
                    if (ProcessImg(obj.sprite, newTileName: newTileName))
                        AddFbObjJsonItem(newTileName: newTileName, weight: obj.weight);
                }
            }
        }

        string GetTileFileName(int? c = null) =>
            (c.HasValue, _additionalTits) switch
            {
                (true, true) => $"{_currentMainTileName}_{_currentTileName}_{c}",
                (true, false) => $"{_currentTileName}_{c}",
                (false, true) => $"{_currentMainTileName}_{_currentTileName}",
                (false, false) => $"{_currentTileName}"
            };

        bool ProcessImg(long number, string newTileName = null)
        {
            var fileNameWithFGAndExt = GetImgName(number);
            if (!File.Exists(fileNameWithFGAndExt))
            {
                WriteLog($"ERROR - File not found: {fileNameWithFGAndExt}");
                return false;
            }

            WriteLog($"File found: {fileNameWithFGAndExt}");

            var newTilePath = Path.Combine(_currentPath, $"{newTileName ?? _currentTileName}{_fileExt}");
            File.Copy(fileNameWithFGAndExt, newTilePath, true);
            WriteLog($"File copied to: {newTilePath}");

            return true;
        }

        string GetImgName(long number)
        {
            var fileName = "";

            Parallel.ForEach(FileNameContainer.FileNameLst, (x, state) =>
            {
                if (x.Key == number)
                {
                    fileName = x.First();
                    state.Stop();
                }
            });

            return fileName;
        }

        void AddFbObjJsonItem(string newTileName, long? weight = null)
        {
            if (String.IsNullOrEmpty(newTileName))
                newTileName = _currentTileName;

            if (weight.HasValue)
                _currentObjJson.fg.Add(new WeightJson { weight = weight.Value, sprite = newTileName });
            else
                _currentObjJson.fg.Add(newTileName);
        }

        void ProcessBgWithPairs()
        {
            if (!_currentTit.bgIntArr.Empty())
                ProcessIntWithPairs(_currentTit.bgIntArr);
            else if (!_currentTit.bgObjArr.Empty())
                ProcessObjWithPairs(_currentTit.bgObjArr);
        }

        void ProcessIntWithPairs(long[] arr)
        {
            foreach (var number in arr)
            {
                string[] bgNames = GetBgNamesFromPairs(number);
                AddBgObjJsonItem(bgNames);
            }
        }

        void ProcessObjWithPairs(WeightObj[] arr)
        {
            if (arr.Length == 1)
            {
                var intArr = arr.Select(x => x.sprite).ToArray();
                ProcessIntWithPairs(intArr);
                return;
            }

            foreach (var obj in arr)
            {
                string[] bgNames = GetBgNamesFromPairs(obj.sprite);
                AddBgObjJsonItem(bgNames, weight: obj.weight);
            }
        }

        string[] GetBgNamesFromPairs(long key)
        {
            if (MyTitCollection.Dictionary.ContainsKey(key))
                return MyTitCollection.Dictionary[key];
            return null;
        }

        void AddBgObjJsonItem(string[] bgNames, long? weight = null)
        {
            if (bgNames.Empty()) return;

            if (weight.HasValue)
                _currentObjJson.bg.AddRange(bgNames.Select(x => new WeightJson { weight = weight.Value, sprite = x }));
            else
                _currentObjJson.bg.AddRange(bgNames);
        }

        void SetObjJsonIdAndAddAdditionalTile()
        {
            if (_currentObjJson.fg.Empty())
            {
                string tilePath = String.Join("/", new string[] { _currentMainTileName, _currentTileName }.Distinct());
                WriteLog($"ERROR - Couldn't found any images for tile '{tilePath}'.");
                return;
            }

            _currentObjJson.id = _currentTileName;

            if (_additionalTits)
                _objJson.AddAdditionalTile(_currentObjJson);
        }

        internal void WriteJsonFile()
        {
            var jsonData = JsonConvert.SerializeObject(_objJson, _settings);
            var jsonPath = Path.Combine(_currentPath, _objJson.fileName);
            File.WriteAllText(jsonPath, jsonData);
            WriteLog($"Json file '{jsonPath}' saved.");
        }
    }
}
