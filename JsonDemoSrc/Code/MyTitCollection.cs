﻿using JsonDemo.Common;
using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Threading.Tasks;

namespace JsonDemo
{
    class MyTitCollection
    {
        const string _dicrionaryKey = "--dictionary";
        static long[] _exclude = { 0L, 957L };

        internal static ConcurrentDictionary<long, string[]> Dictionary;

        internal static void BuildDictionaryFromFile(string[] args)
        {
            if (args.Empty() || args.Length % 2 != 0) return;

            var argsDic = new Dictionary<string, string>();
            for (var i = 0; i < args.Length; i += 2)
            {
                if (!argsDic.ContainsKey(args[i]))
                    argsDic.Add(args[i], args[i + 1]);
            }

            if (argsDic.ContainsKey(_dicrionaryKey))
            {
                var dicPath = argsDic[_dicrionaryKey];
                if (String.IsNullOrEmpty(dicPath)) return;
                if (!File.Exists(dicPath)) return;

                var lines = File.ReadAllLines(dicPath)
                    .Where(line => !String.IsNullOrEmpty(line));

                if (lines.Empty()) return;

                var dic = lines.Select(x => x.Split(new char[] { ':' }, StringSplitOptions.RemoveEmptyEntries))
                    .ToDictionary(x => x[0].ToLong(), x => x.Skip(1).ToArray());

                Dictionary = new ConcurrentDictionary<long, string[]>(dic);
            }
        }

        internal static void BuildDictionaryFromConfigJson(ConfigJson jsonObj)
        {
            var items = jsonObj.tiles_new
                .Where(x => !x.tiles.Empty())
                .SelectMany(x => x.tiles.Select(y => new MyTit(y)))
                .Where(x => x.existID && (x.existFG || x.existBG));

            var dic = items
                .SelectMany(x =>
                {
                    if (x.existFG)
                    {
                        if (!x.fgIntArr.Empty())
                            return x.fgIntArr.Select(y => (key: y, value: x.id));

                        return x.fgObjArr.Select(y => (key: y.sprite, value: x.id));
                    }

                    if (!x.bgIntArr.Empty())
                        return x.bgIntArr.Select(y => (key: y, value: x.id));

                    return x.bgObjArr.Select(y => (key: y.sprite, value: x.id));
                })
                .Where(x => !_exclude.Contains(x.key))
                .GroupBy(
                    x => x.key,
                    x => x.value,
                    (x, y) => new KeyValuePair<long, string[]>(x, y.SelectMany(z => z).ToArray()));

            if (Dictionary == null)
                Dictionary = new ConcurrentDictionary<long, string[]>(dic);
            else
                Parallel.ForEach(dic, kvp => Dictionary.TryAdd(kvp.Key, kvp.Value));
        }
    }
}
