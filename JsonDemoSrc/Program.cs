﻿using JsonDemo.Common;
using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace JsonDemo
{
    class Program
    {
        const string _jsonFile = "input_tileset\\tile_config.json";
        const string _inputDir = "input_objects";
        const string _outputDir = "output_objects";

        static string[] _idList = null;

        static ConfigJson _jsonObj;
        static string _fileNameWithoutExt;
        static string _fileExt;

        static void Main(string[] args)
        {
            IsHelpDisplayed(args);

            Log.CleanLog();

            if (!Validate()) return;

            if (Directory.Exists(_outputDir)) Directory.Delete(_outputDir, true);
            Directory.CreateDirectory(_outputDir);

            MyTitCollection.BuildDictionaryFromFile(args);

            MyTitCollection.BuildDictionaryFromConfigJson(_jsonObj);

            foreach (var newTit in _jsonObj.tiles_new)
            {
                SplitFileNameAndExt(newTit.file);

                Log.WriteLog();
                Log.WriteLog($"File name: {_fileNameWithoutExt}");

                if (newTit.tiles.Empty())
                {
                    Log.WriteLog($"ERROR - Empty tiles");
                    continue;
                }

                Parallel.ForEach(newTit.tiles, tit =>
                {
                    var worker = new TitWorker(tit);

                    if (!worker.ValidateID(idMustContainLst: _idList) || (!worker.ValidateFG() && !worker.ValidateBG()))
                        return;

                    worker.SetFileExtention(_fileExt);

                    foreach (var newTileName in worker.MainId)
                    {
                        worker.SetCurrentTile(newTileName);

                        if (worker.IsInvalidDirectory())
                            continue;

                        worker.SetCurrentPath(Path.Combine(_outputDir, _fileNameWithoutExt, newTileName));

                        worker.CreateJsonObj();

                        worker.LogNewTileName();

                        worker.ProcessMainTit();

                        worker.WriteJsonFile();
                    }
                });
            }

            var merger = new MergeJson(new string[1] { _outputDir }, false);
            merger.MergeJsons();
        }

        static void IsHelpDisplayed(string[] args)
        {
            if (args.Empty()) return;

            for (int i = 0; i < args.Length; i++)
            {
                switch (args[i])
                {
                    case "-?":
                    case "-h":
                    case "--help":
                        DisplayHelp();
                        break;
                    case "--dictionary":
                        i++;
                        break;
                    case "--id-must-contain":
                        string path = args[++i];
                        if (String.IsNullOrEmpty(path))
                            DisplayHelp("Error: path to file containing list of ids is empty.");
                        if (!File.Exists(path))
                            DisplayHelp("Error: file containing list of ids doesn't exists.");
                        _idList = File.ReadAllLines(path);
                        break;
                    default:
                        DisplayHelp($"Invalid argument: {args[i]}");
                        break;
                }
            }
        }

        static void DisplayHelp(string error = null)
        {
            if (!String.IsNullOrEmpty(error))
            {
                Console.WriteLine();
                Console.WriteLine(error);
            }

            Console.WriteLine();
            Console.WriteLine("Usage:");
            Console.WriteLine("  --dictionary <dictionary_path>");
            Console.WriteLine("  --id-must-contain <id_list_path>");
            Console.WriteLine();
            Console.WriteLine("Dictionary example:");
            Console.WriteLine();
            Console.WriteLine("1:dirt");
            Console.WriteLine("2:dirt_2");
            Console.WriteLine("3[:]");
            Console.WriteLine("4:dirt_3");
            Console.WriteLine();

            Environment.Exit(String.IsNullOrEmpty(error) ? 0 : 1);
        }

        static bool Validate()
        {
            if (!Directory.Exists(_inputDir))
            {
                Log.WriteLog($"ERROR - Directory not found: '{_inputDir}'.");
                return false;
            }

            var numPatt = new Regex("_(\\d+)\\.png");
            var fileNameLst = Directory.GetFiles(_inputDir)
                .ToLookup(x => numPatt.Match(x).Groups[1].Value.ToLong());
            if (fileNameLst.Count == 0)
            {
                Log.WriteLog($"ERROR - Directory is empty: '{_inputDir}'.");
                return false;
            }
            else
                FileNameContainer.FileNameLst = Partitioner.Create(fileNameLst);

            if (!File.Exists(_jsonFile))
            {
                Log.WriteLog($"ERROR - File not found: '{_jsonFile}'.");
                return false;
            }

            var jsonData = File.ReadAllText(_jsonFile);
            if (String.IsNullOrEmpty(jsonData))
            {
                Log.WriteLog($"ERROR - File '{_jsonFile}' is empty.");
                return false;
            }

            _jsonObj = JsonConvert.DeserializeObject<ConfigJson>(jsonData);

            if (_jsonObj?.tiles_new.Empty() ?? true)
            {
                Log.WriteLog($"ERROR - File '{_jsonFile}' doesn't contains new tiles.");
                return false;
            }

            return true;
        }

        static void SplitFileNameAndExt(string fullName)
        {
            var fi = new FileInfo(fullName);
            _fileExt = fi.Extension;
            _fileNameWithoutExt = fi.Name.Substring(0, fi.Name.Length - _fileExt.Length);
        }
    }
}
